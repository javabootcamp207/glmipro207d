package com.techno207Proj.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.techno207Proj.model.SupplierModel;
import com.techno207Proj.repository.SupplierRepository;

@Service
@Transactional
public class SupplierService {
	@Autowired
	private SupplierRepository spr;
	
	public List<SupplierModel> listAll() {
		return spr.findAll();
	}
	
	public SupplierModel save(SupplierModel SupplierModel) {
		return spr.save(SupplierModel);
	}
	
	public SupplierModel get(int idsupplier) {
		return spr.findById(idsupplier).get();
	}
	
	public void delete(int id) {
		spr.deleteById(id);
	}
	
	public String CekEmailLama(String mail) {
		return spr.CekEmailLama(mail);
	}
	
}
