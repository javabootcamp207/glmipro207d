package com.techno207Proj.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.techno207Proj.model.SupplierModel;


public interface SupplierRepository extends JpaRepository<SupplierModel,Integer> {

		@Query(value = "SELECT s.email FROM SupplierModel s where s.email = ?1  ")
		String CekEmailLama(String mail);
	
	
}
