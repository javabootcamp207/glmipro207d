package com.techno207Proj.controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.techno207Proj.model.ProductModel;
import com.techno207Proj.model.SupplierModel;
import com.techno207Proj.service.ProductService;
import com.techno207Proj.service.SupplierService;

@Controller
public class ProductController {
	@Autowired
	private ProductService prdc;
	
	@Autowired
	private SupplierService spr;
	
	@RequestMapping("/product")
	public String product() {
		
		return "Product/product";
	}
	
	@RequestMapping("/prdclistbynama/{nama}")
	public String ProdByName(@PathVariable(name = "nama") String nama
							,Model model) {
		List<ProductModel> prdclist = prdc.ProdByName(nama);
		model.addAttribute("prdclist", prdclist);
		return "Product/productlist";
	}
	
	@RequestMapping("/prdclist")
	public String prdclist(Model model) {
		List<ProductModel> prdclist = prdc.ProdList();
		model.addAttribute("prdclist", prdclist);
		return "Product/productlist";
	}
	
	@RequestMapping("/prdcnew")
	public String prdcnew(Model model) {
		
		List<SupplierModel> listSupplier = spr.listAll();
		model.addAttribute("listSupplier", listSupplier);
		return "Product/productnew";
	}
	
	// simpan dan edit data
	@ResponseBody
	@RequestMapping(value = "/saveprdc")
	public String saveprdc(@ModelAttribute("prdcModelDepan") ProductModel ProductModel) {
		String savests = "";
		prdc.save(ProductModel);
		return savests;
	}
	
	@RequestMapping("/prdcdelfrm/{id}")
	public String deletefromprdc(@PathVariable(name = "id") int id, Model model) {
		ProductModel ProductModel = prdc.get(id);
		model.addAttribute("prdcmodeldepan", ProductModel);
		
		
		return "Product/productdelet";
	}
	
	@ResponseBody
	@RequestMapping("/deleteprdc/{id}")
	public String deleteprdc(@PathVariable(name = "id") int id) {
		String delsts = "0";
		
			prdc.delete(id);
	
		return delsts;
	}
	
	@RequestMapping("/prdcedit/{id}")
	public String editfromprdc(@PathVariable(name = "id") int id, Model model) {
		ProductModel ProductModel = prdc.get(id);
		model.addAttribute("prdcmodeldepan", ProductModel);
		
		List<SupplierModel> listSupplier = spr.listAll();
		model.addAttribute("listSupplier", listSupplier);
		return "Product/productedit";
	}
	
}
