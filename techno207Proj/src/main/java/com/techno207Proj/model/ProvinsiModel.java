package com.techno207Proj.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "provinsi")
public class ProvinsiModel {
	@Id
	@Column(name = "id")
	private String id;
	@Column(name = "nama")
	private String nama;
	@Column(name = "id_provinsi")
	private String idprovinsi;
}
